var exports = (module.exports = {});
const admin = require("firebase-admin");
const properties = require("../properties/properties");

exports.function = "wishlist";
exports.api = [];

exports.api.push({
  path: "/addToWishlist",
  post: true,
  secure: false,
  handler: async (req, res) => {
    try {
      let { uid, codigo, nombre, imagen } = req.body;
      let refdoc = admin
        .firestore()
        .collection("wishlist")
        .doc(uid);
      let listDB = await refdoc.get();
      let list = [{ codigo, nombre, imagen, estatus: 1 }];

      if (listDB.exists) {
        list = list.concat(listDB.data().list);
        console.log(listDB.data().list);
        console.log(list);
        refdoc
          .set({ list })
          .then(() => {
            res.status(200).send({
              error: true,
              msg: "Agregado a tu Wishlist"
            });
          })
          .catch(err => {
            res.status(500).send({
              error: true,
              msg: "No se guardo la wishlist",
              detalle: err
            });
          });
      } else {
        refdoc
          .set({ list: [{ codigo, nombre, imagen, estatus: 1 }] })
          .then(() => {
            res.status(200).send({
              error: true,
              msg: "Agregado a tu Wishlist"
            });
          })
          .catch(err => {
            res.status(500).send({
              error: true,
              msg: "No se guardo la wishlist",
              detalle: err
            });
          });
      }
    } catch (err) {
      console.log(err);
      res.status(500).send({
        error: false,
        msg: "Ocurrio un error inesperado",
        detalle: err
      });
    }
  }
});

exports.api.push({
  path: "/getWishlist",
  post: true,
  secure: false,
  handler: async (req, res) => {
    try {
      let { uid } = req.body;
      let refdoc = admin
        .firestore()
        .collection("wishlist")
        .doc(uid);
      let listDB = await refdoc.get();
      res.status(200).send({
        error: true,
        msg: "wishlist recuperada correctamente",
        wishlist: listDB.data().list
      });
    } catch (err) {
      console.log(err);
      res.status(500).send({
        error: false,
        msg: "Ocurrio un error inesperado",
        detalle: err
      });
    }
  }
});


exports.api.push({
  path: "/addToWishListN",
  post: true,
  secure: false,
  handler: async (req, res) => {
    let { uid, codigo, nombre, imagen } = req.body;
    admin.firestore().collection("wishlistN").doc(codigo).get().then((rs) => {
      if (rs.exists) {
        admin.firestore().collection("wishlistN").doc(codigo).update({
          [uid]: {
            uid, codigo, nombre, imagen
          }
        }).then((rs) => {
          res.status(200).send({ error: false, msg: "Articulo Agregado." });
        }).catch((error) => {
          res.status(500).send({ error: true, msg: "No se logro guardar el articulo." });
        })
      } else {
        admin.firestore().collection("wishlistN").doc(codigo).set({
          [uid]: {
            uid, codigo, nombre, imagen
          }
        }).then((rs) => {
          res.status(200).send({ error: false, msg: "Articulo Agregado." });
        }).catch((error) => {
          res.status(500).send({ error: true, msg: "No se logro guardar el articulo." });
        })
      }
    })
  }
});


exports.api.push({
  path: "/getMyWishList",
  post: true,
  secure: false,
  handler: async (req, res) => {
    let { uid } = req.body;
    admin.firestore().collection("wishlistN").get().then(rs=>{
        let wishlist = [];

        let docs = rs.docs;

        console.log("docs",docs);
        for(var doc of docs){
          let data = doc.data();
          data.id = doc.id;
          console.log(data[uid]);
          if(data[uid]){
            wishlist.push(data[uid]);
          }
        }

        res.status(200).send(wishlist);

    });
  } 
});

exports.api.push({
  path: "/getTokenWish",
  post: true,
  secure: false,
  handler: async (req, res) => {
    let { codigo } = req.body;
    admin.firestore().collection("wishlistN").doc(codigo).get().then(rs=>{
        let wishlist = [];

        if(rs.exists){
          wishlist = Object.keys(rs.data());

        }

        res.status(200).send(wishlist);

    });
  } 
});
