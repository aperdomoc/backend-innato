var exports = (module.exports = {});
const admin = require("firebase-admin");
const properties = require("../properties/properties");

exports.function = "refacciones";
exports.api = [];

exports.api.push({
  path: "/getRefacciones",
  post: false,
  secure: false,
  handler: async (req, res) => {

    admin.firestore().collection("refacciones").get().then((result)=>{
      let refacciones = [];

       if(!result.empty){
         let docs = result.docs;

         for(var doc of docs){
           let data = doc.data();
           data.id = doc.id;

           refacciones.push(data);
         }
       }

      res.status(200).send(refacciones);
    });
  }
});



exports.api.push({
  path: "/getRefacByModelo",
  post: true,
  secure: false,
  handler: async (req, res) => {
    try {
      let { modelo } = req.body;
      let categorias = admin.firestore().collection("refacciones");
      let refacciones = await categorias.where("modelo", "==", modelo).get();
      var resultado = [];

      for (var i = 0; i < refacciones.docs.length; i++) {
        let data = refacciones.docs[i].data();
        resultado.push(data);
      }

      res.status(200).send({
        error: false,
        msg: "datos obtenidos correctamente",
        refacciones: resultado
      });
    } catch (err) {
      res.status(500).send({
        error: true,
        msg: "Ocurrio un error inesperado"
      });
    }
  }
});

 
exports.api.push({
    path: "/saveRefaccion",
    post: true,
    secure: false,
    handler: async (req, res) => {
      try {
          let {categoria, codigo, 
            imagen, modelo, 
            nombre, precio, sistema} = req.body;

            admin.firestore().collection('refacciones').add({
                categoria, codigo, imagen, modelo, nombre, precio, sistema
            }).then(()=>{
                res.status(200).send({
                    error:true,
                    msg:"Refaccion guardada correctamente"
                })
            }).catch(err=>{
                res.status(500).send({
                    error:true,
                    msg:"ocurrio un error al guardar",
                    detalle:err
                })
            })
            ;

      }catch(err){
          res.status(500).send({
              error:true,
              msg:"Ocurrio un error inesperado"
          })
      }
    }
});