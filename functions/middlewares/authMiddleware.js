var admin = require("firebase-admin");

var exports = module.exports = {};

exports.validateFirebaseToken=(req, res, next)=> {

    if(!req.headers.authorization){
		return res.status(403).send({
            error:true,
			msg: 'La petición no tiene la cabezera de autenticación'
		});
    }
   
    let id_token =  req.headers.authorization;

 
    admin.auth().verifyIdToken(id_token).then((decode) => {
        req.uid = decode.uid;
        next();
    }).catch((err) => {
        res.status(403).send({error:true,msg:"Sin autorización"});
    });
    
}
