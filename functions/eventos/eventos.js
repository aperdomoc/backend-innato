var exports = (module.exports = {});
const admin = require("firebase-admin");
const properties = require("../properties/properties");

exports.function = "eventos";
exports.api = [];

exports.api.push({
    "path": "/addEvento",
    "post": true,
    "secure": false,
    "handler": async (req, res) => {
        let { eventos } = req.body;
        let agregados = [];
        for (var evento of eventos) {
            let agregado = await addEvento(evento);
            agregado ?
                agregados.push(`Evento: ${evento.nombre} Agregado`) :
                agregados.push(`Evento: ${evento.nombre} No Agregado`)
        }
        res.status(200).send({ agregados });
    }
});

function addEvento(evento) {
    return new Promise((resolve, reject) => {
        admin.firestore().collection('eventos').add(evento).then(() => {
            resolve({ added: true });
        }).catch((error) => {
            resolve({ added: false });
        })
    });
}

exports.api.push({
    "path": "/getEvento",
    "post": false,
    "secure": false,
    "handler": (req, res) => {
        admin.firestore().collection('eventos').get().then((result) => {

            let docs = result.docs;

            let eventos = [];

            for (usr of docs) {
                let data = usr.data();
                data.id = usr.id;
                eventos.push(data);
            }

            res.status(200).send({ eventos });

        }).catch((error) => {
            res.status(500).send({ msg: "Ocurrio un error", error });
        });
    }
});