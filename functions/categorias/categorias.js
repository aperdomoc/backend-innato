var exports = module.exports = {};
const admin = require('firebase-admin');
const properties = require('../properties/properties');

exports.function = "categories";
exports.api = [];



exports.api.push({
    "path": "/addCategories",
    "post": true,
    "secure": false,
    "handler": async (req, res) => {

        let {categories} = req.body;
        let agregados = [];



        for(var categorie of categories){
            let agregado = await addCategorie(categorie);
                agregado ? 
                agregados.push(`Categorie ${categorie.nombre} Agregada`) :
                agregados.push(`Categorie ${categorie.nombre} No Agregada`)
        }

        res.status(200).send({agregados});

       
    }
});


function addCategorie(categorie){
    return new Promise((resolve,reject)=>{
        admin.firestore().collection('categorias').add(categorie).then(() => {
            resolve({added:true});
        }).catch((error) => {
           resolve({added:false});
        })
    });
}


exports.api.push({
    "path": "/getCategories",
    "post": false,
    "secure": false,
    "handler": (req, res) => {
        admin.firestore().collection('categorias').get().then((result) => {

            let docs = result.docs;

            let categorias = [];

            for (usr of docs) {
                let data = usr.data();
                data.id = usr.id;
                categorias.push(data);
            }

            res.status(200).send({ categorias });

        }).catch((error) => {
            res.status(500).send({ msg: "Ocurrio un error", error });
        });
    }
});