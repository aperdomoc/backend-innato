var exports = (module.exports = {});
const admin = require("firebase-admin");
const properties = require("../properties/properties");

exports.function = "models";
exports.api = [];

exports.api.push({
  path: "/addModels",
  post: true,
  secure: false,
  handler: async (req, res) => {
    let { models } = req.body;
    let agregados = [];

    for (var model of models) {
      let agregado = await addModel(model);
      agregado
        ? agregados.push(`Model ${model.nombre} Agregada`)
        : agregados.push(`Model ${model.nombre} No Agregada`);
    }

    res.status(200).send({ agregados });
  }
});

function addModel(model) {
  return new Promise((resolve, reject) => {
    admin
      .firestore()
      .collection("modelos")
      .add(model)
      .then(() => {
        resolve({ added: true });
      })
      .catch(error => {
        resolve({ added: false });
      });
  });
}

exports.api.push({
  path: "/getModels",
  post: false,
  secure: false,
  handler: (req, res) => {
    admin
      .firestore()
      .collection("modelos")
      .get()
      .then(result => {
        let docs = result.docs;

        let modelos = [];

        for (usr of docs) {
          let data = usr.data();
          data.id = usr.id;
          modelos.push(data);
        }

        res.status(200).send({ modelos });
      })
      .catch(error => {
        res.status(500).send({ msg: "Ocurrio un error", error });
      });
  }
});

exports.api.push({
  path: "/getModelsByCategoria",
  post: true,
  secure: false,
  handler: async (req, res) => {
    try {
      let { categoria } = req.body;
      let categorias = admin.firestore().collection("modelos");
      let modelos = await categorias.where("Categoria", "==", categoria).get();
      var resultado = [];

      for (var i = 0; i < modelos.docs.length; i++) {
        let data = modelos.docs[i].data();
        resultado.push(data);
      }
      if (resultado.length > 0) {
        res.status(200).send({
          error: false,
          msg: "Modelos recuperados correctamente",
          modelos: resultado
        });
      } else {
        res.status(500).send({
          error: true,
          msg: "No existen modelos para la categoria elegida"
        });
      }
    } catch (err) {
      res.status(500).send({
        error: true,
        msg: "Ocurrio un error al consultar modelos"
      });
    }
  }
});

exports.api.push({
  path: "/getModelbyNombre",
  post: true,
  secure: false,
  handler: async (req, res) => {
    try {
      let { nombre } = req.body;
      let categorias = admin.firestore().collection("modelos");
      let modelos = await categorias.where("nombre", "==", nombre).get();
      var resultado;

      for (var i = 0; i < modelos.docs.length; i++) {
        let data = modelos.docs[i].data();
        resultado = data;
      }
      if (resultado) {
        res.status(200).send({
          error: false,
          msg: "modelo recuperado correctamente",
          modelo: resultado
        });
      } else {
        res.status(404).send({
          error: true,
          msg: "No existe el modelo consultado"
        });
      }
    } catch (err) {
      res.status(500).send({
        error: true,
        msg: "Ocurrio un error al recuperar datos"
      });
    }
  }
});
