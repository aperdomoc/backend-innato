const initClass = require("./initializer.js");
const admin = require('firebase-admin');
const functions = require("firebase-functions");
const adminCert = require("./adminCert.json");
const dbURL = require("./properties/properties.js");


admin.initializeApp({
  credential: admin.credential.cert(adminCert),
  databaseURL: dbURL.URL_SERVICE_FB
}
);

var modulos = [];

modulos.push(require("./user/users.js"));
modulos.push(require('./ejemplos/ejemplos.js'));
modulos.push(require('./categorias/categorias.js'));
modulos.push(require('./models/models.js'));
modulos.push(require('./refacciones/refacciones.js'));
modulos.push(require('./wishlist/wishlist.js'));
modulos.push(require('./eventos/eventos.js'));
modulos.push(require('./eventlist/eventlist.js'));
modulos.push(require('./accesorios/accesorios.js'));

var apis = initClass.initFunctions(modulos);
for (var api of apis) {
  exports[api.function] = api.handler;
}


exports.sendNotification = functions.firestore.document("accesorios/{id}").onUpdate(snap => {
  const accesorio = snap.after.data();
  console.log(accesorio);
  const payload = {
    notification: {
      title: `Nueva actualización para el ${accesorio.nombre}`,
      body: 'Ve la actualización para el producto que sigues.',
      badge: '1',
      sound: 'default',
      image: accesorio.imagen
    }
  };

  admin.firestore().collection("wishlistN").doc(accesorio.codigo).get().then(async rs => {
    let tokens = [];
      if (rs.exists) {
        let objkeys = Object.keys(rs.data());
        console.log(objkeys);
        for (var uid of objkeys) {
          let token = await getUserToken(uid);
          if (token) {
            tokens.push(token);
          }
        }

        return admin.messaging().sendToDevice(tokens, payload);
      }else{
        return "";
      }
    
  });
});

exports.sendNotificationRefacciones = functions.firestore.document("refacciones/{id}").onUpdate(snap => {
  const accesorio = snap.after.data();
  console.log(accesorio);
  const payload = {
    notification: {
      title: `Nueva actualización para el ${accesorio.nombre}`,
      body: 'Ve la actualización para el producto que sigues.',
      badge: '1',
      sound: 'default',
      image: accesorio.imagen
    }
  };

  admin.firestore().collection("wishlistN").doc(accesorio.codigo).get().then(async rs => {
    let tokens = [];
      if (rs.exists) {
        let objkeys = Object.keys(rs.data());
        console.log(objkeys);
        for (var uid of objkeys) {
          let token = await getUserToken(uid);
          if (token) {
            tokens.push(token);
          }
        }
      }
    return admin.messaging().sendToDevice(tokens, payload);
  });
});

exports.eventNotification = functions.firestore.document("eventos/{id}").onCreate(snap => {
  const evento = snap.data()
  console.log(evento);
  const payload = {
    notification: {
      title: `Nueva evento cerca de tí`,
      body: `${evento.descripcion}`,
      badge: '1',
      sound: 'default',
      image: evento.imagen
    }
  };



  return admin.messaging().sendToDevice("fnsz2_H4OHQ:APA91bH0Z_-d84Xs-4OjOHvRGWucj49aUULTTjsk8kREu9A1Sc9VCKt4RA84aLHbiSLsRUx4bhC8NG6Nvf88JM4MvsDiRI8PHA4t4pluI_CjuinlaaiKJuzDH6qArA8z-vsdqbBZKcJk", payload);
});

exports.eventNotificationUpdate = functions.firestore.document("eventos/{id}").onUpdate(async snap => {
  const evento = snap.after.data()
  const eventId = snap.after.id;
  console.log(evento);
  const payload = {
    notification: {
      title: `El evento ${evento.nombre} se ha actualizado`,
      body: `${evento.descripcion}`,
      badge: '1',
      sound: 'default',
      image: evento.imagen
    }
  };

  admin.firestore().collection("eventlist").doc(eventId).get().then(async (rs) => {
    let tokens = [];
    if (rs.exists) {
      let objkeys = Object.keys(rs.data());
      console.log(objkeys);
      for (var uid of objkeys) {
        let token = await getUserToken(uid);
        if (token) {
          tokens.push(token);
        }
      }
    }
    return admin.messaging().sendToDevice(tokens, payload);

  });


});


function getUserToken(uid) {
  return new Promise((resolve, reject) => {
    admin.firestore().collection("token").doc(uid).get().then((rs) => {
      if (rs.exists) {
        let data = rs.data();
        console.log(data.token);
        resolve(data.token);
      } else {
        resolve(false)
      }
    });
  });
}