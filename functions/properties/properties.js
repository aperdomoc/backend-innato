'use-strict'

module.exports = {
    URL_SERVICE_FB : "https://hackaton-salinas-innato.firebaseio.com",
    ERROR_CODES: {
        'auth/invalid-email': {msg:"Capture un correo electrónico válido.",error:true},
        'auth/invalid-display-name': {msg:"Capture nombre y apellidos válidos.",error:true},
        'auth/invalid-photo-url':{msg:"Capture una imagen válida.",error:true},
        'auth/email-already-exists':{msg:"Ya existe un usuario registrado con este correo electrónico.",error:true},
        'auth/user-not-found':{msg:"Usuario no encontrado.",error:true},
        'auth/invalid-password':{msg:"Capture una contraseña válida.",error:true}
    }
}