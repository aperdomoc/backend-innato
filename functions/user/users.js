var exports = (module.exports = {});
const admin = require("firebase-admin");
const properties = require("../properties/properties");

exports.function = "users";
exports.api = [];

exports.api.push({
  path: "/createUser",
  post: true,
  secure: false,
  handler: (req, res) => {
    let { email, password, name } = req.body;
    let valido = true;

    if (!email || email.trim() === "") {
      valido = false;
      res
        .status(400)
        .send({ msg: "Capture un correo electrónico.", error: true });
    } else if (!password || password.trim() === "") {
      valido = false;
      res.status(400).send({ msg: "Capture su contraseña.", error: true });
    } else if (!name || name.trim() === "") {
      valido = false;
      res.status(400).send({ msg: "Capture su nombre.", error: true });
    } else if (name.length > 50) {
      valido = false;
      res.status(400).send({
        msg: "Su nombre no pueden superar los 50 caracteres.",
        error: true
      });
    }

    if (valido) {
      let dataUser = {
        email,
        emailVerified: false,
        password: password,
        displayName: name.trim(),
        disabled: false
      };

      admin
        .auth()
        .createUser(dataUser)
        .then(userRecord => {
          res.status(200).send({ msg: "Felicidades tu usuario se ha creado." });
        })
        .catch(err => {
          if (properties.ERROR_CODES[err.code]) {
            let error = properties.ERROR_CODES[err.code];
            res.status(500).send({ msg: error.msg, error: true });
          } else {
            res.status(500).send({
              msg: "No se logro crear su usuario, intente nuevamente.",
              error: true
            });
          }
        });
    }
  }
});

exports.api.push({
  path: "/profile",
  post: true,
  secure: false,
  handler: async (req, res) => {
    let { uid, modelo, kilometraje, fnacimiento, direccion } = req.body;
    let saved_data = await guardaMoto(
      uid,
      modelo,
      kilometraje,
      fnacimiento,
      direccion
    );
    if (!saved_data.detalle) {
      res.status(200).send({
        error: false,
        msg: "datos guardados correctamente"
      });
    } else {
      re.status(500).send({
        error: true,
        msg: "Ocurrió no se pudo guardar los datos"
      });
    }
  }
});

exports.api.push({
  path: "/getProfile",
  post: true,
  secure: false,
  handler: async (req, res) => {
    try {
      let { uid } = req.body;
      let profile = admin
        .firestore()
        .collection("usuarios")
        .doc(uid);
      var documento = await profile.get();
      console.log(documento.data());

      if (documento.exists) {
        res.status(200).send({
          error: false,
          msg: "Perfil recuperado correctamente",
          perfil: documento.data()
        });
      } else {
        res.status(500).send({
          error: true,
          msg: "Perfil no encontrado"
        });
      }
    } catch (err) {
      res.status(500).send({
        error: true,
        msg: "Ocurrio un error inesperado"
      });
    }
  }
});

exports.api.push({
  path: "/getProfile",
  post: true,
  secure: false,
  handler: async (req, res) => {
    try {
      let {uid, token} = req.body;
      let reftoken = admin.firestore().collection('notification').doc(uid);
      reftoken.set({token}).then(()=>{
        res.status(200).send({
          error:true,
          msg:"Token guardado correctamente"
        })
      }).then(err=>{
        res.status(500).send({
          error:true,
          msg:"no se pudo guardar el toke",
          detalle:err
        })
      });
    }catch(err){
      res.status(500).send({
        error:true,
        msg:"Ocurrio un error inesperad",
        detalle:err
      })
    }
  }
});

async function guardaMoto(uid, modelo, kilometraje, fnacimiento, direccion) {
  return new Promise((resolve, reject) => {
    admin
      .firestore()
      .collection("usuarios")
      .doc(uid)
      .set({
        moto: [{ modelo, kilometraje }],
        fnacimiento: fnacimiento,
        direccion: direccion
      })
      .then(() => {
        resolve({ saved: true });
      })
      .catch(err => {
        console.log(err);
        reject({ saved: false, detalle: err });
      });
  });
}


exports.api.push({
  path: "/saveToken",
  post: true,
  secure: false,
  handler: async (req, res) => {
    let { uid, token } = req.body;
    let saved_data = await guardaToken(
      uid,
      token,
    );
    if (!saved_data.detalle) {
      res.status(200).send({
        error: false,
        msg: "datos guardados correctamente"
      });
    } else {
      re.status(500).send({
        error: true,
        msg: "Ocurrió no se pudo guardar los datos"
      });
    }
  }
});

async function guardaToken(uid, token) {
  return new Promise((resolve, reject) => {
    admin
      .firestore()
      .collection("token")
      .doc(uid)
      .set({
        token
      })
      .then(() => {
        resolve({ saved: true });
      })
      .catch(err => {
        console.log(err);
        reject({ saved: false, detalle: err });
      });
  });
}
