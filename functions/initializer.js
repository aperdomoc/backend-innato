const express = require("express");
const cors = require("cors")({origin: true});
const functions = require("firebase-functions");
const authMiddleware = require("./middlewares/authMiddleware");

var exports = module.exports = {};

exports.initFunctions = function(modulos) {
	var appFunctions = [];
    
	for (var mod of modulos) {
	    
		var app = express();
    	app.use(cors);
        
		for (var api of mod.api) {
			if (api.post) {

				if(api.secure){
					app.post(api.path, authMiddleware.validateFirebaseToken, api.handler);
				}else{
					if(api.secure){
						app.post(api.path, authMiddleware.validateFirebaseToken, api.handler);
					}else{
						app.post(api.path, api.handler);
					}
				}

				
			} else {
				if(api.secure){
					app.get(api.path, authMiddleware.validateFirebaseToken,api.handler);
				}else{
					app.get(api.path,api.handler);
				}
				
			}
		}
        
		appFunctions.push({ "function": mod.function, "handler": functions.https.onRequest(app)});
		exports[mod.function] = functions.https.onRequest(app);
	}
    
	return appFunctions;
};