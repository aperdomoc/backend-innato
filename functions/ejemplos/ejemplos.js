var exports = module.exports = {};
const admin = require('firebase-admin');
const properties = require('../properties/properties');

exports.function = "ejemplos";
exports.api = [];


// exports.api.push({
//     "path": "/create",
//     "post": true,
//     "secure": false,
//     "handler": (req, res) => {

//     }
// });



exports.api.push({
    "path": "/create",
    "post": true,
    "secure": false,
    "handler": (req, res) => {
        let { nombre, edad } = req.body;

        let valido = true;

        if (!nombre || nombre.trim() == "") {
            valido = false;
            res.status(500).send({ msg: "ingresa un nombre" });
        }

        if (!edad) {
            edad = 0;
        }


        if (valido) {
            admin.firestore().collection('usuarios').add({ nombre, edad }).then(() => {
                res.status(200).send({ msg: `El usuario ${nombre} se registro con esta edad ${edad}` });
            }).catch((error) => {
                res.status(500).send({ msg: "Ocurrio un error", error });
            })
        }
    }
});

exports.api.push({
    "path": "/read",
    "post": false,
    "secure": false,
    "handler": (req, res) => {
        admin.firestore().collection('usuarios').get().then((result) => {

            let docs = result.docs;

            let usuarios = [];

            for (usr of docs) {
                let data = usr.data();
                data.id = usr.id;
                usuarios.push(data);
            }

            res.status(200).send({ usuarios });

        }).catch((error) => {
            res.status(500).send({ msg: "Ocurrio un error", error });
        });
    }
});


exports.api.push({
    "path": "/update",
    "post": true,
    "secure": false,
    "handler": (req, res) => {
        let { id, nombre, edad } = req.body;

        admin.firestore().collection('usuarios').doc(id).update({ nombre, edad }).then(() => {
            res.status(200).send({ msg: `El usuario ${nombre}  con esta edad ${edad} se actualizo.` });
        }).catch((error) => {
            res.status(500).send({ msg: "Ocurrio un error", error });
        })

    }
});


exports.api.push({
    "path": "/delete",
    "post": true,
    "secure": false,
    "handler": (req, res) => {
        let { id} = req.body;

        admin.firestore().collection('usuarios').doc(id).delete().then(() => {
            res.status(200).send({ msg: `El usuario fue eliminado ALV.` });
        }).catch((error) => {
            res.status(500).send({ msg: "Ocurrio un error", error });
        })
    }
});


exports.api.push({
    "path": "/datosAdicionales",
    "post": true,
    "secure": false,
    "handler": async (req, res) => {

        let { apellido, curp,id } = req.body;

        let valido = true;

        if (!id || id.trim() == "") {
            valido = false;
            res.status(500).send({ msg: "ingresa su id" });
        }else if (!apellido || apellido.trim() == "") {
            valido = false;
            res.status(500).send({ msg: "ingresa su nombre" });
        }else  if (!curp || curp.trim() == "") {
            valido = false;
            res.status(500).send({ msg: "ingresa su curp" });
        }
       let validaUsuario =await validUser(id);
       console.log('RESULTADO',validaUsuario);
        if (valido) {
            if(validaUsuario){
                admin.firestore().collection('adicionales').add({ apellido, curp,id }).then(() => {
                    res.status(200).send({ msg: `Los datos adicionales se registraron` });
                }).catch((error) => {
                    res.status(500).send({ msg: "Ocurrio un error", error });
                })
            }else{
                res.status(500).send({ msg: "El usuario no existe" });
            }
           
        }
    }
});



exports.api.push({
    "path": "/readAdicionales",
    "post": false,
    "secure": false,
    "handler": (req, res) => {
        admin.firestore().collection('usuarios').get().then(async (result) => {

            let docs = result.docs;

            let usuarios = [];

            for (usr of docs) {
                let data = usr.data();
                data.id = usr.id;
                data.adicionales =  await getAdicionales(usr.id);
                usuarios.push(data);
            }

            res.status(200).send({ usuarios });

        }).catch((error) => {
            res.status(500).send({ msg: "Ocurrio un error", error });
        });
    }
});


function getAdicionales(id){
    return new Promise((resolve,reject)=>{
        admin.firestore().collection('adicionales').where('id',"==",id).get().then((result)=>{
           let data = result.docs[0].data()
           resolve(data);
        }).catch((error)=>{
            resolve({});
        })
    });
}

function validUser(id){
    return new Promise((resolve,reject)=>{
        admin.firestore().collection('usuarios').doc(id).get().then((result)=>{
            console.log(result.exists);
            if(result.exists){
                resolve(true);
            }else{
                resolve(false);
            }
        }).catch((error)=>{
            resolve(false);
        })
    });
}