var exports = (module.exports = {});
const admin = require("firebase-admin");
const properties = require("../properties/properties");

exports.function = "eventlist";
exports.api = [];



exports.api.push({
    path: "/addEventList",
    post: true,
    secure: false,
    handler: async (req, res) => {
      let {uid,eventId} = req.body;

        admin.firestore().collection("eventlist").doc(eventId).get().then((rs)=>{
            if(rs.exists){
                admin.firestore().collection("eventlist").doc(eventId).update({[uid]:uid}).then((rs)=>{
                    res.status(200).send({error:false,msg:"Evento Agregado."});
                   }).catch((error)=>{
                       res.status(500).send({error:true,msg:"No se logro guardar el evento."});
                   })
            }else{
                admin.firestore().collection("eventlist").doc(eventId).set({[uid]:uid}).then((rs)=>{
                    res.status(200).send({error:false,msg:"Evento Agregado."});
                   }).catch((error)=>{
                       res.status(500).send({error:true,msg:"No se logro guardar el evento."});
                   })
            }
        })     
    }
  });


  exports.api.push({
    path: "/getUserOfEvent",
    post: true,
    secure: false,
    handler: async (req, res) => {
      let {eventId} = req.body;

       admin.firestore().collection("eventlist").doc(eventId).get().then(async (rs)=>{
           let usuarios = [];
        if(rs.exists){
            let objkeys = Object.keys(rs.data());

            console.log(objkeys);

            for(var uid of objkeys){
                let token = await getUserToken(uid);
                let data = {
                    uid,
                    token 
                };

                if(data.token){
                    usuarios.push(token);
                }
            }

        }

        res.status(200).send({usuarios})
       });
    }
  });

  function getUserToken(uid){
      return new Promise((resolve,reject)=>{
        admin.firestore().collection("token").doc(uid).get().then((rs)=>{
            if(rs.exists){
                let data = rs.data();
                console.log(data.token);
                resolve(data.token);
            }else{
                resolve(false)
            }
        });
      });
  }