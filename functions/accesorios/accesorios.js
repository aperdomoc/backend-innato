var exports = module.exports = {};
const admin = require('firebase-admin');
const properties = require('../properties/properties');

exports.function = "accesorios";
exports.api = [];


exports.api.push({
    "path": "/getAccesorios",
    "post": false,
    "secure": false,
    "handler": (req, res) => {
        admin.firestore().collection('accesorios').get().then((result) => {

            let docs = result.docs;

            let accesorios = [];

            for (usr of docs) {
                let data = usr.data();
                data.id = usr.id;
                accesorios.push(data);
            }

            res.status(200).send({ accesorios });

        }).catch((error) => {
            res.status(500).send({ msg: "Ocurrio un error", error });
        });
    }
});